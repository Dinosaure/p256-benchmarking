#include <caml/mlvalues.h>
#include <caml/bigarray.h>


CAMLprim value ml_Hacl_Ed25519_verify(value pk, value m, value sig) {
    return Val_bool(Hacl_Ed25519_verify(Caml_ba_data_val(pk),
                                        Caml_ba_array_val(m)->dim[0],
                                        Caml_ba_data_val(m),
                                        Caml_ba_data_val(sig)));
}

CAMLprim value ml_Hacl_P256_ecdsa_verif_without_hash(value pk, value m, value r, value s) {
    return Val_bool(Hacl_P256_ecdsa_verif_without_hash(Caml_ba_array_val(m)->dim[0],
                                                       Caml_ba_data_val(m),
                                                       Caml_ba_data_val(pk),
                                                       Caml_ba_data_val(r),
                                                       Caml_ba_data_val(s)));
}

